extern crate md5;
use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().map(Result::unwrap);
    let root = lines.next().unwrap();

    let mut password = ['x'; 8];
    let mut remaining = 8;
    for i in 0.. {
        let key = format!("{}{}", root, i);
        let digest = format!("{:x}", md5::compute(&key));
        if digest.starts_with("00000") {
            if let Ok(pos) = digest[5..6].parse::<usize>() {
                if password.get(pos) == Some(&'x') {
                    password[pos] = digest.bytes().nth(6).unwrap() as char;
                    remaining -= 1;
                }
            }
        }

        if remaining == 0 {
            break;
        }
    }

    for ch in &password {
        print!("{}", ch);
    }
    println!("");
}
