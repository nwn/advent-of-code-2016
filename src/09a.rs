use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().map(Result::unwrap);
    let line = lines.next().unwrap();

    let mut decompressed = String::new();
    let mut idx = 0;
    while idx < line.len() {
        if let Some(next_marker) = line[idx..].find('(') {
            let start_marker = idx + next_marker + 1;
            let end_marker = idx + line[idx..].find(')').unwrap();
            let marker = &line[start_marker..end_marker];
            let parts: Vec<_> = marker.split('x').map(|part| part.parse::<usize>().unwrap()).collect();
            let len = parts[0];
            let rep = parts[1];

            let start_rep = end_marker + 1;
            let end_rep = start_rep + len;

            decompressed.push_str(&line[idx..start_marker-1]);
            decompressed.push_str(&line[start_rep..end_rep].to_string().repeat(rep));
            idx = end_rep;
        } else {
            decompressed.push_str(&line[idx..]);
            idx = line.len();
        }
    }

    println!("{}", decompressed.len());
}
