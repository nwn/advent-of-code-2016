use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut pos = vec![];
    for line in lines {
        if pos.is_empty() {
            pos.resize(line.len(), vec![0; 26]);
        }

        for (i, ch) in line.bytes().enumerate() {
            let freqs = &mut pos[i];
            freqs[(ch - b'a') as usize] += 1;
        }
    }

    let mut message = String::new();
    for pos in pos {
        let most_common = pos.iter()
                             .enumerate()
                             .filter(|(_, freq)| **freq > 0)
                             .map(|(i, freq)| (freq, i))
                             .min().unwrap();
        message.push((most_common.1 as u8 + b'a') as char);
    }
    println!("{}", message);
}
