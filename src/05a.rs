extern crate md5;
use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().map(Result::unwrap);
    let root = lines.next().unwrap();

    let mut password = String::new();
    for i in 0.. {
        let key = format!("{}{}", root, i);
        let digest = format!("{:x}", md5::compute(&key));
        if digest.starts_with("00000") {
            password.push_str(&digest[5..6]);
        }

        if password.len() == 8 {
            break;
        }
    }
    println!("{}", password);
}
