use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut nodes = vec![];
    for line in lines {
        let words: Vec<_> = line.split_whitespace()
                                .flat_map(|word| word.split_terminator('x'))
                                .flat_map(|word| word.split_terminator('y'))
                                .flat_map(|word| word.split_terminator('-'))
                                .flat_map(|word| word.split_terminator('T'))
                                .flat_map(|word| word.split_terminator('%'))
                                .filter_map(|word| word.parse::<usize>().ok())
                                .collect();
        if words.is_empty() {
            continue;
        }

        let node = Node {
            _x: words[0],
            _y: words[1],
            _size: words[2],
            used: words[3],
            avail: words[4],
            _percent: words[5],
        };
        nodes.push(node);
    }

    let mut count = 0;
    for i in 0..nodes.len() {
        for j in i+1..nodes.len() {
            if nodes[i].used > 0 && nodes[i].used <= nodes[j].avail {
                count += 1;
            }
            if nodes[j].used > 0 && nodes[j].used <= nodes[i].avail {
                count += 1;
            }
        }
    }

    println!("{}", count);
}

struct Node {
    _x: usize,
    _y: usize,
    _size: usize,
    used: usize,
    avail: usize,
    _percent: usize,
}
