use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    for line in lines {
        if !check_real(&line) {
            continue;
        }
        let sector: u32 = {
            let start = line.find(|ch: char| ch.is_ascii_digit()).unwrap();
            let end = line.rfind(|ch: char| ch.is_ascii_digit()).unwrap();
            line[start..=end].parse().unwrap()
        };
        let line = &line[..line.rfind('-').unwrap()];
        let line = rot_n(line, sector);

        if line.find("north").is_some() {
            println!("{}", sector);
        }
    }
}

fn rot_n(line: &str, n: u32) -> String {
    let mut res = String::new();
    for ch in line.bytes() {
        if ch == b'-' {
            res.push(' ');
            continue;
        }
        let mut decypher = (ch - b'a') as u32;
        decypher += n;
        decypher %= 26;
        res.push((decypher as u8 + b'a') as char);
    }
    res
}

fn check_real(line: &str) -> bool {
    let mut freqs = [0; 26];
    for ch in line.chars() {
        if ch == '-' { continue; }
        if !ch.is_ascii_lowercase() { break; }

        freqs[(ch as u8 - b'a') as usize] += 1;
    }
    let check_sum = {
        let start = line.find('[').unwrap() + 1;
        let end = line.find(']').unwrap();
        &line[start..end]
    };

    let mut freq_pairs = [(0,'a'); 26];
    for i in 0..freqs.len() {
        // Negate frequencies to obtain decreasing order
        freq_pairs[i] = (-freqs[i], (i as u8 + b'a') as char);
    }
    freq_pairs.sort();

    let mut chk = String::new();
    for i in 0..5 {
        chk.push(freq_pairs[i].1);
    }

    chk == check_sum
}
