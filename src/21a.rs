use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut password = String::from("abcdefgh").into_bytes();
    for line in lines {
        let words: Vec<_> = line.split_whitespace().collect();

        match (words[0], words[1]) {
            ("swap", "position") => {
                let x = words[2].parse::<usize>().unwrap();
                let y = words[5].parse::<usize>().unwrap();
                password.swap(x, y);
            },
            ("swap", "letter") => {
                let a = password.iter().position(|&ch| ch == words[2].as_bytes()[0]).unwrap();
                let b = password.iter().position(|&ch| ch == words[5].as_bytes()[0]).unwrap();
                password.swap(a, b);
            },
            ("rotate", "left") => {
                let x = words[2].parse::<usize>().unwrap();
                password.rotate_left(x);
            },
            ("rotate", "right") => {
                let x = words[2].parse::<usize>().unwrap();
                password.rotate_right(x);
            },
            ("rotate", "based") => {
                let x = password.iter().position(|&ch| ch == words[6].as_bytes()[0]).unwrap();
                let x = (x + 1 + if x >= 4 { 1 } else { 0 }) % password.len();
                password.rotate_right(x);
            },
            ("reverse", "positions") => {
                let x = words[2].parse::<usize>().unwrap();
                let y = words[4].parse::<usize>().unwrap();
                password[x..=y].reverse();
            },
            ("move", "position") => {
                let x = words[2].parse::<usize>().unwrap();
                let y = words[5].parse::<usize>().unwrap();
                let ch = password.remove(x);
                password.insert(y, ch);
            },
            _ => panic!("Unknown command: {}", line),
        }
    }

    println!("{}", std::str::from_utf8(&password).unwrap());
}
