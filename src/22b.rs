use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut target_node = (0, 0);
    let mut empty_node = (0, 0);
    for line in lines {
        let words: Vec<_> = line.split_whitespace()
                                .flat_map(|word| word.split_terminator('x'))
                                .flat_map(|word| word.split_terminator('y'))
                                .flat_map(|word| word.split_terminator('-'))
                                .flat_map(|word| word.split_terminator('T'))
                                .flat_map(|word| word.split_terminator('%'))
                                .filter_map(|word| word.parse::<usize>().ok())
                                .collect();
        if words.is_empty() {
            continue;
        }

        let x = words[0];
        let y = words[1];
        let used = words[3];

        if used == 0 {
            empty_node = (y, x);
        }
        if x > target_node.1 {
            target_node.1 = x;
        }
    }

    // By inspection, we know that the empty node must move along the walls in
    // an optimal path. We therefore calculate the steps needed to move the
    // empty node to the left wall, up to the top wall, and across to the
    // target. Then we shift the target along to top wall to the goal node.
    let steps = empty_node.1 +           // move empty node left
                empty_node.0 +           // move empty node up
                target_node.1 +          // move empty node right
                5 * (target_node.1 - 1); // shift target left

    println!("{}", steps);
}
