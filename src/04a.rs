use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut sum = 0;
    for line in lines {
        let mut freqs = [0; 26];
        for ch in line.chars() {
            if ch == '-' { continue; }
            if !ch.is_ascii_lowercase() { break; }

            freqs[(ch as u8 - b'a') as usize] += 1;
        }
        let sector: u32 = {
            let start = line.find(|ch: char| ch.is_ascii_digit()).unwrap();
            let end = line.rfind(|ch: char| ch.is_ascii_digit()).unwrap();
            line[start..=end].parse().unwrap()
        };
        let check_sum = {
            let start = line.find('[').unwrap() + 1;
            let end = line.find(']').unwrap();
            &line[start..end]
        };

        let mut freq_pairs = [(0,'a'); 26];
        for i in 0..freqs.len() {
            // Negate frequencies to obtain decreasing order
            freq_pairs[i] = (-freqs[i], (i as u8 + b'a') as char);
        }
        freq_pairs.sort();

        let mut chk = String::new();
        for i in 0..5 {
            chk.push(freq_pairs[i].1);
        }

        if chk == check_sum {
            sum += sector;
        }
    }

    println!("{}", sum);
}
