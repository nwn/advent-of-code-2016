use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut code = String::new();
    let (mut x, mut y): (i32, i32) = (1, 1);
    for line in lines {
        for ch in line.chars() {
            use std::cmp::{min, max};
            match ch {
                'U' => y = max(0, y - 1),
                'D' => y = min(2, y + 1),
                'L' => x = max(0, x - 1),
                'R' => x = min(2, x + 1),
                _   => panic!("Unexpected input: {}", ch),
            }
        }

        let num = 3 * y + x + 1;
        code.push((num + '0' as i32) as u8 as char);
    }
    println!("{}", code);
}
