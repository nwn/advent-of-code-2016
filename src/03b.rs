use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().map(Result::unwrap).peekable();

    let mut count = 0;
    while lines.peek().is_some() {
        let line_group = [lines.next().unwrap(), lines.next().unwrap(), lines.next().unwrap()];
        let vals: Vec<_> = line_group.iter().map(|line| line.split_whitespace()
                                                            .map(|word| word.parse::<u32>().unwrap())
                                                            .collect::<Vec<_>>()
                                                ).collect();

        for col in 0..3 {
            let is_triangle = vals[0][col] + vals[1][col] > vals[2][col] &&
                              vals[1][col] + vals[2][col] > vals[0][col] &&
                              vals[2][col] + vals[0][col] > vals[1][col];

            if is_triangle {
                count += 1;
            }
        }
    }

    println!("{}", count);
}
