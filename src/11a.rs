use std::io::{self, prelude::*};
use std::collections::{HashSet, HashMap, VecDeque};
use std::hash::Hash;

const NUM_FLOORS: usize = 4;
const NUM_ELEMS: usize = 5;

fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut indexer = Indexer::new();
    let mut gens = [[false; NUM_ELEMS]; NUM_FLOORS];
    let mut chips = [[false; NUM_ELEMS]; NUM_FLOORS];

    for (floor, line) in lines.enumerate() {
        let words: Vec<_> = line.split_whitespace().collect();

        for pair in words.windows(2) {
            if pair[1].starts_with("generator") {
                let elem = indexer.get_id(pair[0].to_owned());
                gens[floor][elem] = true;
            } else if pair[1].starts_with("microchip") {
                let end = pair[0].find('-').unwrap();
                let elem = indexer.get_id(pair[0][..end].to_owned());
                chips[floor][elem] = true;
            }
        }
    }

    let mut queue = VecDeque::new();
    let mut visited = HashSet::new();

    let initial = State { cur_floor: 0, gens, chips };
    queue.push_back((0, initial));
    visited.insert(initial);

    while !queue.is_empty() {
        let (steps, state) = queue.pop_front().unwrap();
        if state.is_complete() {
            println!("{}", steps);
            break;
        }

        for state in state.neighbours() {
            if !visited.contains(&state) {
                queue.push_back((steps + 1, state));
                visited.insert(state);
            }
        }
    }
}

struct Indexer<T: Hash + Eq> {
    seen: HashMap<T, usize>,
}
impl<T: Hash + Eq> Indexer<T> {
    fn new() -> Self {
        Self { seen: HashMap::new() }
    }
    fn get_id(&mut self, val: T) -> usize {
        if let Some(&id) = self.seen.get(&val) {
            id
        } else {
            let id = self.seen.len();
            self.seen.insert(val, id);
            id
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Hash)]
struct State {
    cur_floor: usize,
    gens: [[bool; NUM_ELEMS]; NUM_FLOORS],
    chips: [[bool; NUM_ELEMS]; NUM_FLOORS],
}
impl State {
    fn is_complete(&self) -> bool {
        self.gens[NUM_FLOORS - 1].iter().all(|x| *x) &&
        self.chips[NUM_FLOORS - 1].iter().all(|x| *x) &&
        self.cur_floor == NUM_FLOORS - 1
    }
    fn neighbours(&self) -> Vec<Self> {
        let mut neighbours = vec![];

        let prev_floor = self.cur_floor;
        let mut next_floors = vec![];
        if prev_floor > 0 { next_floors.push(prev_floor - 1); }
        if prev_floor + 1 < NUM_FLOORS { next_floors.push(prev_floor + 1); }

        for next_floor in next_floors {
            // 1G, 0C
            for elem in 0..NUM_ELEMS {
                if !self.gens[prev_floor][elem] { continue; }

                let mut gens = self.gens;
                gens[prev_floor][elem] = false;
                gens[next_floor][elem] = true;

                if is_exposed(&gens[prev_floor], &self.chips[prev_floor]) { continue; }
                if is_exposed(&gens[next_floor], &self.chips[next_floor]) { continue; }

                neighbours.push(Self {
                    cur_floor: next_floor,
                    gens,
                    chips: self.chips,
                });
            }

            // 2G, 0C
            for elem1 in 0..NUM_ELEMS {
                if !self.gens[prev_floor][elem1] { continue; }
                for elem2 in elem1+1..NUM_ELEMS {
                    if !self.gens[prev_floor][elem2] { continue; }

                    let mut gens = self.gens;
                    gens[prev_floor][elem1] = false;
                    gens[prev_floor][elem2] = false;
                    gens[next_floor][elem1] = true;
                    gens[next_floor][elem2] = true;

                    if is_exposed(&gens[prev_floor], &self.chips[prev_floor]) { continue; }
                    if is_exposed(&gens[next_floor], &self.chips[next_floor]) { continue; }

                    neighbours.push(Self {
                        cur_floor: next_floor,
                        gens,
                        chips: self.chips,
                    });
                }
            }

            // 0G, 1C
            for elem in 0..NUM_ELEMS {
                if !self.chips[prev_floor][elem] { continue; }

                let mut chips = self.chips;
                chips[prev_floor][elem] = false;
                chips[next_floor][elem] = true;

                if is_exposed(&self.gens[prev_floor], &chips[prev_floor]) { continue; }
                if is_exposed(&self.gens[next_floor], &chips[next_floor]) { continue; }

                neighbours.push(Self {
                    cur_floor: next_floor,
                    gens: self.gens,
                    chips,
                });
            }

            // 2G, 0C
            for elem1 in 0..NUM_ELEMS {
                if !self.chips[prev_floor][elem1] { continue; }
                for elem2 in elem1+1..NUM_ELEMS {
                    if !self.chips[prev_floor][elem2] { continue; }

                    let mut chips = self.chips;
                    chips[prev_floor][elem1] = false;
                    chips[prev_floor][elem2] = false;
                    chips[next_floor][elem1] = true;
                    chips[next_floor][elem2] = true;

                    if is_exposed(&self.gens[prev_floor], &chips[prev_floor]) { continue; }
                    if is_exposed(&self.gens[next_floor], &chips[next_floor]) { continue; }

                    neighbours.push(Self {
                        cur_floor: next_floor,
                        gens: self.gens,
                        chips,
                    });
                }
            }

            // 1G, 1C
            for elem in 0..NUM_ELEMS {
                if !self.gens[prev_floor][elem] || !self.chips[prev_floor][elem] { continue; }

                let mut gens = self.gens;
                let mut chips = self.chips;
                gens[prev_floor][elem] = false;
                chips[prev_floor][elem] = false;
                gens[next_floor][elem] = true;
                chips[next_floor][elem] = true;

                if is_exposed(&gens[prev_floor], &chips[prev_floor]) { continue; }
                if is_exposed(&gens[next_floor], &chips[next_floor]) { continue; }

                neighbours.push(Self {
                    cur_floor: next_floor,
                    gens,
                    chips,
                });
            }
        }

        neighbours
    }
}

fn is_exposed(gens: &[bool; NUM_ELEMS], chips: &[bool; NUM_ELEMS]) -> bool {
    if gens.iter().all(|x| !x) {
        return false;
    }

    for elem in 0..NUM_ELEMS {
        if chips[elem] && !gens[elem] {
            return true;
        }
    }
    false
}
