use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut count = 0;
    for line in lines {
        if find_abba(line.as_bytes(), &mut 0) == Some(0) {
            count += 1;
        }
    }
    println!("{}", count);
}

fn find_abba(str: &[u8], ind: &mut usize) -> Option<usize> {
    let mut found = false;
    while *ind <= str.len() - 4 {
        if str[*ind] == b'[' {
            *ind += 1;
            if let Some(depth) = find_abba(str, ind) {
                return Some(depth + 1);
            }
        } else if str[*ind] == b']' {
            break;
        } else {
            let aa = str[*ind] == str[*ind + 3];
            let bb = str[*ind + 1] == str[*ind + 2];
            let ab = str[*ind] != str[*ind + 1];
            let ba = str[*ind].is_ascii_lowercase() && str[*ind + 1].is_ascii_lowercase();

            if aa && bb && ab && ba {
                found = true;
            }
        }

        *ind += 1;
    }

    if found {
        Some(0)
    } else {
        None
    }
}
