extern crate md5;

use std::io::{self, prelude::*};
use std::collections::HashMap;

fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().map(Result::unwrap);
    let salt = lines.next().unwrap();

    let mut wanted = HashMap::new();
    for ch in b'0'..=b'9' { wanted.insert(ch, vec![]); }
    for ch in b'a'..=b'f' { wanted.insert(ch, vec![]); }

    let mut keys = vec![];
    let mut limit = None;
    for idx in 0.. {
        let input = format!("{}{}", salt, idx);
        let hash = format!("{:x}", md5::compute(&input));

        if let Some(ch) = find_run(&hash, 5) {
            for i in wanted.get_mut(&ch).unwrap().drain(..) {
                if idx <= i + 1000 {
                    keys.push(i);
                    if keys.len() == 64 {
                        limit = Some(idx + 1000);
                    }
                }
            }
        }
        if let Some(ch) = find_run(&hash, 3) {
            wanted.get_mut(&ch).unwrap().push(idx);
        }

        if limit.is_some() && limit.unwrap() <= idx {
            break;
        }
    }
    keys.sort();
    println!("{}", keys[63]);
}

fn find_run(str: &str, n: usize) -> Option<u8> {
    'outer: for window in str.as_bytes().windows(n) {
        for idx in 1..window.len() {
            if window[0] != window[idx] {
                continue 'outer;
            }
        }
        return Some(window[0]);
    }
    None
}
