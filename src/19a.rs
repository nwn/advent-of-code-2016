use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().map(Result::unwrap);
    let num = lines.next().unwrap().parse::<usize>().unwrap();

    let mut elves = Vec::with_capacity(num);
    for i in 1..=num {
        elves.push(i);
    }

    // Advance once around the circle, eliminating half of the elves each time.
    // Those with even parity survive. Those with odd parity are eliminated.
    // Index 0 is considered to have the same parity as the circle.
    while elves.len() > 1 {
        let mut new_elves = Vec::with_capacity(elves.len() / 2);

        if elves.len() % 2 == 0 {
            new_elves.push(elves[0]);
        }
        for elf in (2..elves.len()).step_by(2) {
            new_elves.push(elves[elf]);
        }

        elves = new_elves;
    }

    println!("{}", elves[0]);
}
