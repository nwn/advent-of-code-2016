use std::io::{self, prelude::*};
use std::collections::HashSet;
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut count = 0;
    for line in lines {
        let imbalance = partial_imbalance(line.as_bytes());
        let aba = find_aba(line.as_bytes());

        let (aba, bab) = {
            let mut sup = HashSet::new();
            let mut hyp = HashSet::new();
            for ind in aba {
                if imbalance[ind] == 0 {
                    sup.insert((line.as_bytes()[ind], line.as_bytes()[ind + 1]));
                } else {
                    hyp.insert((line.as_bytes()[ind + 1], line.as_bytes()[ind]));
                }
            }
            (sup, hyp)
        };

        if aba.intersection(&bab).count() > 0 {
            count += 1;
        }
    }
    println!("{}", count);
}

fn partial_imbalance(str: &[u8]) -> Vec<i32> {
    let mut vec = Vec::with_capacity(str.len());
    let mut imbalance = 0;
    for ind in 0..str.len() {
        match str[ind] {
            b'[' => imbalance += 1,
            b']' => imbalance -= 1,
            _ => (),
        }
        vec.push(imbalance);
    }
    vec
}

fn find_aba(str: &[u8]) -> Vec<usize> {
    let mut vec = Vec::new();
    for (ind, window) in str.windows(3).enumerate() {
        let aa = window[0] == window[2];
        let ab = window[0] != window[1];
        let ba = window[0].is_ascii_lowercase() && window[1].is_ascii_lowercase();

        if aa && ab && ba {
            vec.push(ind);
        }
    }
    vec
}
