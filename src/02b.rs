use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut code = String::new();
    let (mut x, mut y): (i32, i32) = (-2, 0);
    for line in lines {
        for ch in line.chars() {
            use std::cmp::{min, max};
            match ch {
                'U' => y = max(y - 1, -2 + x.abs()),
                'D' => y = min(y + 1,  2 - x.abs()),
                'L' => x = max(x - 1, -2 + y.abs()),
                'R' => x = min(x + 1,  2 - y.abs()),
                _   => panic!("Unexpected input: {}", ch),
            }
        }

        code.push(match y {
            -2 => '1',
            -1 => ('2' as u8 + (x + 1) as u8) as char,
             0 => ('5' as u8 + (x + 2) as u8) as char,
             1 => ('A' as u8 + (x + 1) as u8) as char,
             2 => 'D',
            _ => unreachable!(),
        });
    }
    println!("{}", code);
}
