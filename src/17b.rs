extern crate md5;

use std::io::{self, prelude::*};
use std::collections::VecDeque;

const DIM: usize = 4;

fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().map(Result::unwrap);
    let path = lines.next().unwrap().into_bytes();
    let root_len = path.len();

    let mut queue = VecDeque::new();
    queue.push_back(((0, 0), path));

    let mut longest = 0;
    while !queue.is_empty() {
        let (pos, mut path) = queue.pop_front().unwrap();
        if pos == (DIM-1, DIM-1) {
            if path.len() - root_len > longest {
                longest = path.len() - root_len;
            }
            continue;
        }

        let doors = open_doors(&path);
        if doors[0] && pos.0 > 0 {
            path.push(b'U');
            queue.push_back(((pos.0 - 1, pos.1), path.clone()));
            path.pop();
        }
        if doors[1] && pos.0 + 1 < DIM {
            path.push(b'D');
            queue.push_back(((pos.0 + 1, pos.1), path.clone()));
            path.pop();
        }
        if doors[2] && pos.1 > 0 {
            path.push(b'L');
            queue.push_back(((pos.0, pos.1 - 1), path.clone()));
            path.pop();
        }
        if doors[3] && pos.1 + 1 < DIM {
            path.push(b'R');
            queue.push_back(((pos.0, pos.1 + 1), path.clone()));
            path.pop();
        }
    }

    println!("{}", longest);
}

fn open_doors(path: &[u8]) -> [bool; 4] {
    let digest = md5::compute(path);
    let digest: [u8; 16] = digest.into();
    [
        digest[0] >> 4 >= 0x0b,
        digest[0] & 0xf >= 0x0b,
        digest[1] >> 4 >= 0x0b,
        digest[1] & 0xf >= 0x0b,
    ]
}
