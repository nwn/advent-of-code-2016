use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().map(Result::unwrap);
    let line = lines.next().unwrap();

    println!("{}", decompress(&line));
}

fn decompress(str: &str) -> usize {
    let mut decompressed = 0;
    let mut idx = 0;
    while idx < str.len() {
        if let Some(next_marker) = str[idx..].find('(') {
            let start_marker = idx + next_marker + 1;
            let end_marker = idx + str[idx..].find(')').unwrap();
            let marker = &str[start_marker..end_marker];
            let parts: Vec<_> = marker.split('x').map(|part| part.parse::<usize>().unwrap()).collect();
            let len = parts[0];
            let rep = parts[1];

            let start_rep = end_marker + 1;
            let end_rep = start_rep + len;

            decompressed += next_marker;
            decompressed += rep * decompress(&str[start_rep..end_rep]);
            idx = end_rep;
        } else {
            decompressed += str.len() - idx;
            idx = str.len();
        }
    }

    decompressed
}
