use std::io::{self, prelude::*};
use std::collections::{HashSet, VecDeque};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut checkpoints = vec![];
    let mut grid = vec![];
    for (row, line) in lines.enumerate() {
        checkpoints.extend(line.chars().enumerate()
                                       .filter(|(_idx, ch)| ch.is_ascii_digit())
                                       .map(|(idx, ch)| (ch, row, idx) ));
        grid.push(line.into_bytes());
    }
    checkpoints.sort();
    let checkpoints: Vec<_> = checkpoints.into_iter().map(|(_ch, row, col)| (row, col)).collect();

    // Find shortest paths between all checkpoints using BFS
    let mut dist = vec![vec![1_000_000_000; checkpoints.len()]; checkpoints.len()];
    for source in 0..dist.len() {
        let mut visited = HashSet::new();
        let mut queue = VecDeque::new();
        queue.push_back((0, checkpoints[source]));

        while !queue.is_empty() {
            let (depth, (row, col)) = queue.pop_front().unwrap();
            if !visited.insert((row, col)) { continue; }

            let ch = grid[row][col];
            if (ch as char).is_ascii_digit() {
                let dest = (ch - b'0') as usize;
                if dist[source][dest] > depth {
                    dist[source][dest] = depth;
                }
            }

            if grid[row - 1][col] != b'#' { queue.push_back((depth + 1, (row - 1, col))); }
            if grid[row + 1][col] != b'#' { queue.push_back((depth + 1, (row + 1, col))); }
            if grid[row][col - 1] != b'#' { queue.push_back((depth + 1, (row, col - 1))); }
            if grid[row][col + 1] != b'#' { queue.push_back((depth + 1, (row, col + 1))); }
        }
    }

    // Find the shortest route among all permutations
    let mut shortest_path = 1_000_000_000;
    let mut order: Vec<usize> = (1..checkpoints.len()).collect();
    for_permutations(&mut order, &mut |order| {
        let mut length = dist[0][order[0]];
        for pair in order.windows(2) {
            length += dist[pair[0]][pair[1]];
        }
        if length < shortest_path {
            shortest_path = length;
        }
    });

    println!("{}", shortest_path);
}

fn for_permutations(arr: &mut [usize], func: &mut FnMut(&[usize])) {
    fn recur(arr: &mut [usize], pos: usize, func: &mut FnMut(&[usize])) {
        if pos + 1 == arr.len() {
            func(arr);
        } else {
            for i in pos..arr.len() {
                arr.swap(pos, i);
                recur(arr, pos + 1, func);
                arr.swap(pos, i);
            }
        }
    }
    recur(arr, 0, func);
}
