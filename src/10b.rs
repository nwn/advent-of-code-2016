use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut bots = vec![];
    let mut edges = vec![];
    let mut ready = vec![];
    let mut outputs = vec![];
    for line in lines {
        let words: Vec<_> = line.split_whitespace().collect();
        if words[0] == "value" {
            let bot = words[5].parse::<usize>().unwrap();
            let chip = words[1].parse().unwrap();

            if bot >= bots.len() {
                bots.resize(bot + 1, vec![]);
            }
            bots[bot].push(chip);
            if bots[bot].len() == 2 {
                ready.push(bot);
            }
        } else {
            let bot = words[1].parse::<usize>().unwrap();
            let lo_t = words[5] == "bot";
            let lo_n = words[6].parse().unwrap();
            let hi_t = words[10] == "bot";
            let hi_n = words[11].parse().unwrap();

            if bot >= edges.len() {
                edges.resize(bot + 1, (false, 0, false, 0));
            }
            edges[bot] = (lo_t, lo_n, hi_t, hi_n);

            for (t, n) in &[(true, bot), (lo_t, lo_n), (hi_t, hi_n)] {
                if *t && *n >= bots.len() {
                    bots.resize(n + 1, vec![]);
                } else if !t && *n >= outputs.len() {
                    outputs.resize(n + 1, vec![]);
                }
            }
        }
    }

    while !ready.is_empty() {
        let bot = ready.pop().unwrap();
        let mut vals = [0,0];
        vals.copy_from_slice(&bots[bot]);
        vals.sort();

        let edge = edges[bot];
        for (t, n, v) in &[(edge.0, edge.1, vals[0]), (edge.2, edge.3, vals[1])] {
            if *t {
                bots[*n].push(*v);
                if bots[*n].len() == 2 {
                    ready.push(*n);
                }
            } else {
                outputs[*n].push(*v);
            }
        }
    }

    println!("{}", outputs[0][0] * outputs[1][0] * outputs[2][0]);
}
