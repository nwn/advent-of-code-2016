use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let line = stdin.lock().lines().map(Result::unwrap).next().unwrap();
    let words: Vec<_> = line.split_whitespace().flat_map(|word| word.split_terminator(',')).collect();

    let (mut x, mut y) = (0, 0);
    let mut dir = Up;
    for word in words {
        match &word[..1] {
            "L" => dir.turn_left(),
            "R" => dir.turn_right(),
            _ => panic!("Unexpected intruction: {}", word),
        }
        let dist: i32 = word[1..].parse().unwrap();
        match dir {
            Up    => y += dist,
            Down  => y -= dist,
            Left  => x -= dist,
            Right => x += dist,
        }
    }

    println!("{}", x.abs() + y.abs());
}

use Dir::*;
enum Dir { Up, Down, Left, Right, }
impl Dir {
    fn turn_left(&mut self) {
        match *self {
            Up    => *self = Left,
            Down  => *self = Right,
            Left  => *self = Down,
            Right => *self = Up,
        }
    }
    fn turn_right(&mut self) {
        match *self {
            Up    => *self = Right,
            Down  => *self = Left,
            Left  => *self = Up,
            Right => *self = Down,
        }
    }
}
