use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut program = vec![];
    for line in lines {
        let words: Vec<_> = line.split_whitespace().collect();
        match words[0] {
            "cpy" => {
                let dest = to_reg(words[2]);
                if let Ok(lit) = words[1].parse::<i32>() {
                    program.push(Cpy(Lit(lit), dest));
                } else {
                    let src = to_reg(words[1]);
                    program.push(Cpy(Reg(src), dest));
                }
            },
            "inc" => {
                program.push(Inc(to_reg(words[1])));
            },
            "dec" => {
                program.push(Dec(to_reg(words[1])));
            },
            "jnz" => {
                let amnt = words[2].parse::<isize>().unwrap();
                if let Ok(lit) = words[1].parse::<i32>() {
                    program.push(Jnz(Lit(lit), amnt));
                } else {
                    let reg = to_reg(words[1]);
                    program.push(Jnz(Reg(reg), amnt));
                }
            },
            _ => panic!("Unknown instruction: {}", line),
        }
    }

    let mut registers = [0; 4];
    let mut pc = 0;
    while let Some(instr) = program.get(pc as usize) {
        let mut step = 1;
        match *instr {
            Cpy(Lit(lit), dest) => registers[dest] = lit,
            Cpy(Reg(src), dest) => registers[dest] = registers[src],
            Inc(reg)            => registers[reg] += 1,
            Dec(reg)            => registers[reg] -= 1,
            Jnz(Lit(lit), amnt) => if lit != 0 { step = amnt; },
            Jnz(Reg(reg), amnt) => if registers[reg] != 0 { step = amnt; },
        }
        pc += step;
    }

    println!("{}", registers[0]);
}

fn to_reg(str: &str) -> usize {
    (str.as_bytes()[0] - b'a') as usize
}

use Val::*;
enum Val {
    Lit(i32),
    Reg(usize),
}

use Instr::*;
enum Instr {
    Cpy(Val, usize),
    Inc(usize),
    Dec(usize),
    Jnz(Val, isize),
}
