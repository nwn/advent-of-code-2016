use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut count = 0;
    for line in lines {
        let mut lens: Vec<_> = line.split_whitespace().map(|word| word.parse::<u32>().unwrap()).collect();

        let is_triangle = lens[0] + lens[1] > lens[2] &&
                          lens[1] + lens[2] > lens[0] &&
                          lens[2] + lens[0] > lens[1];

        if is_triangle {
            count += 1;
        }
    }

    println!("{}", count);
}
