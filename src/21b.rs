use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut commands = vec![];
    for line in lines {
        let words: Vec<_> = line.split_whitespace().collect();

        let command = match (words[0], words[1]) {
            ("swap", "position") => {
                let x = words[2].parse::<usize>().unwrap();
                let y = words[5].parse::<usize>().unwrap();
                SwapPos(x, y)
            },
            ("swap", "letter") => {
                let a = words[2].as_bytes()[0];
                let b = words[5].as_bytes()[0];
                SwapLet(a, b)
            },
            ("rotate", "left") => {
                let x = words[2].parse::<usize>().unwrap();
                RotLeft(x)
            },
            ("rotate", "right") => {
                let x = words[2].parse::<usize>().unwrap();
                RotRight(x)
            },
            ("rotate", "based") => {
                let a = words[6].as_bytes()[0];
                RotLet(a)
            },
            ("reverse", "positions") => {
                let x = words[2].parse::<usize>().unwrap();
                let y = words[4].parse::<usize>().unwrap();
                Reverse(x, y)
            },
            ("move", "position") => {
                let x = words[2].parse::<usize>().unwrap();
                let y = words[5].parse::<usize>().unwrap();
                Move(x, y)
            },
            _ => panic!("Unknown command: {}", line),
        };
        commands.push(command);
    }

    let mut password = String::from("fbgdceah").into_bytes();
    for command in commands.into_iter().rev() {
        match command {
            SwapPos(x, y) => {
                password.swap(x, y);
            },
            SwapLet(a, b) => {
                let x = password.iter().position(|&ch| ch == a).unwrap();
                let y = password.iter().position(|&ch| ch == b).unwrap();
                password.swap(x, y);
            },
            RotLeft(x) => {
                password.rotate_right(x);
            },
            RotRight(x) => {
                password.rotate_left(x);
            },
            RotLet(a) => {
                let x = password.iter().position(|&ch| ch == a).unwrap();
                if x == 0 {
                    password.rotate_left(1);
                } else if x % 2 == 0 {
                    password.rotate_right((6 - x) / 2);
                } else if x % 2 == 1 {
                    password.rotate_left(x / 2 + 1);
                }
            },
            Reverse(x, y) => {
                password[x..=y].reverse();
            },
            Move(x, y) => {
                let ch = password.remove(y);
                password.insert(x, ch);
            },
        }
    }

    println!("{}", std::str::from_utf8(&password).unwrap());
}

use Command::*;
enum Command {
    SwapPos(usize, usize),
    SwapLet(u8, u8),
    RotLeft(usize),
    RotRight(usize),
    RotLet(u8),
    Reverse(usize, usize),
    Move(usize, usize),
}
