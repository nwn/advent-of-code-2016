fn main() {
    // By inspection, the assembly given is simply a slow calculation of
    // 12! + 75 * 88. Why can do better.
    let mut a = 12;
    for b in 1..a {
        a *= b;
    }
    a += 75 * 88;
    println!("{}", a);
}
