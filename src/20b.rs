use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut ranges = vec![];
    for line in lines {
        let mut vals = line.split('-');
        let low = vals.next().unwrap().parse::<u32>().unwrap();
        let high = vals.next().unwrap().parse::<u32>().unwrap();

        ranges.push((low, high));
    }
    ranges.sort();

    let mut count = 0;
    let mut candidate = 0;
    for (low, high) in ranges {
        if candidate < low {
            count += low - candidate;
        }
        if candidate <= high {
            if high == std::u32::MAX {
                break;
            }
            candidate = high + 1;
        }
    }

    println!("{}", count);
}
