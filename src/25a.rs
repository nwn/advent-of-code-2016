use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut program = vec![];
    for line in lines {
        let words: Vec<_> = line.split_whitespace().collect();
        match words[0] {
            "cpy" => {
                let dest = to_reg(words[2]);
                if let Ok(lit) = words[1].parse::<i32>() {
                    program.push(Cpy(Lit(lit), dest));
                } else {
                    let src = to_reg(words[1]);
                    program.push(Cpy(Reg(src), dest));
                }
            },
            "inc" => {
                program.push(Inc(to_reg(words[1])));
            },
            "dec" => {
                program.push(Dec(to_reg(words[1])));
            },
            "jnz" => {
                let amnt = words[2].parse::<isize>().unwrap();
                if let Ok(lit) = words[1].parse::<i32>() {
                    program.push(Jnz(Lit(lit), amnt));
                } else {
                    let reg = to_reg(words[1]);
                    program.push(Jnz(Reg(reg), amnt));
                }
            },
            "out" => {
                if let Ok(lit) = words[1].parse::<i32>() {
                    program.push(Out(Lit(lit)));
                } else {
                    let reg = to_reg(words[1]);
                    program.push(Out(Reg(reg)));
                }
            }
            _ => panic!("Unknown instruction: {}", line),
        }
    }

    // By inspecting the assembly, we see the algorithm is as follows:
    //   a += 7 * 362;
    //   loop {
    //       let mut b = a;
    //       while b != 0 {
    //          println!("{}", b & 1);
    //          b >>= 1;
    //       }
    //   }
    // Therefore, we simply find the least integer with alternating bits
    // greater than 7 * 362, yielding 0b101010101010 - 7 * 362 = 196.
    let mut registers = [196, 0, 0, 0];
    let mut pc = 0;
    while let Some(instr) = program.get(pc as usize) {
        let mut step = 1;
        match *instr {
            Cpy(Lit(lit), dest) => registers[dest] = lit,
            Cpy(Reg(src), dest) => registers[dest] = registers[src],
            Inc(reg)            => registers[reg] += 1,
            Dec(reg)            => registers[reg] -= 1,
            Jnz(Lit(lit), amnt) => if lit != 0 { step = amnt; },
            Jnz(Reg(reg), amnt) => if registers[reg] != 0 { step = amnt; },
            Out(Lit(lit))       => println!("{}", lit),
            Out(Reg(reg))       => println!("{}", registers[reg]),
        }
        pc += step;
    }
}

fn to_reg(str: &str) -> usize {
    (str.as_bytes()[0] - b'a') as usize
}

use Val::*;
enum Val {
    Lit(i32),
    Reg(usize),
}

use Instr::*;
enum Instr {
    Cpy(Val, usize),
    Inc(usize),
    Dec(usize),
    Jnz(Val, isize),
    Out(Val),
}
