use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().map(Result::unwrap);
    let num = lines.next().unwrap().parse::<usize>().unwrap();

    let mut elves = Vec::with_capacity(num);
    for i in 1..=num {
        elves.push(i);
    }

    // Advance one third of the the way around the circle, eliminating that
    // many elves each time. Those before the half-way mark are safe. Only one
    // third of those after the half-way mark survive, determined by the parity
    // of the circle.
    while elves.len() > 1 {
        let mut new_elves = Vec::with_capacity(2*elves.len()/3);

        let end = div_round_up(elves.len(), 3);
        let rem = (elves.len() - 1) % 2 + 1;

        for elf in end..elves.len()/2 {
            new_elves.push(elves[elf]);
        }
        for elf in elves.len()/2..elves.len() {
            let from_half = elf - elves.len() / 2;
            if from_half % 3 == rem {
                new_elves.push(elves[elf]);
            }
        }
        for elf in 0..end {
            new_elves.push(elves[elf]);
        }

        elves = new_elves;
    }

    println!("{}", elves[0]);
}

fn div_round_up(num: usize, denom: usize) -> usize {
    (num + denom - 1) / denom
}
