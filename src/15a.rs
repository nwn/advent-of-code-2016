use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut discs = vec![];
    for line in lines {
        let words: Vec<_> = line.split_whitespace().collect();
        let num_pos = words[3].parse::<usize>().unwrap();
        let start_pos = words[11].split_terminator('.').next().unwrap().parse::<usize>().unwrap();
        discs.push((start_pos, num_pos));
    }

    'outer: for t0 in 0.. {
        for (dt, (offset, modulo)) in discs.iter().enumerate() {
            if (offset + t0 + dt + 1) % modulo != 0 {
                continue 'outer;
            }
        }
        println!("{}", t0);
        break;
    }
}
