use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().map(Result::unwrap);
    let mut state = lines.next().unwrap().into_bytes();

    let disc_len = 272;
    while state.len() < disc_len {
        let mut copy = state.clone();
        copy.reverse();
        for b in &mut copy {
            *b = if *b == b'0' { b'1' } else { b'0' };
        }
        state.push(b'0');
        state.append(&mut copy);
    }
    state.truncate(disc_len);

    while state.len() % 2 == 0 {
        let mut checksum = vec![];
        for pair in state.chunks(2) {
            if pair[0] == pair[1] {
                checksum.push(b'1');
            } else {
                checksum.push(b'0');
            }
        }
        state = checksum;
    }

    println!("{}", String::from_utf8(state).unwrap());
}
