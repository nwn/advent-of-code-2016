use std::io::{self, prelude::*};
use std::collections::{HashSet, VecDeque};
fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().map(Result::unwrap);
    let constant = lines.next().unwrap().parse::<usize>().unwrap();

    let mut queue = VecDeque::new();
    let mut visited = HashSet::new();
    queue.push_back((0, (1, 1)));

    let target = (31, 39);
    while !queue.is_empty() {
        let (depth, (x, y)) = queue.pop_front().unwrap();
        if (x, y) == target {
            println!("{}", depth);
            break;
        }

        let mut moves = vec![];
        if x > 0 {
            moves.push((x - 1, y));
        }
        if y > 0 {
            moves.push((x, y - 1));
        }
        moves.push((x + 1, y));
        moves.push((x, y + 1));

        for (x, y) in moves {
            if !visited.insert((x, y)) { continue; }

            if (x*x + 3*x + 2*x*y + y + y*y + constant).count_ones() % 2 == 0 {
                queue.push_back((depth + 1, (x, y)));
            }
        }
    }
}
