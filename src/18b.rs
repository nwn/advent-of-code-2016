use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().map(Result::unwrap);

    let mut row: Vec<_> = lines.next().unwrap().chars().map(|ch| ch == '^').collect();
    let len = row.len();

    let mut count = row.iter().filter(|b| !*b).count();
    for _ in 1..400_000 {
        let mut new_row = vec![false; len];
        new_row[0] = is_trap(&[false, row[0], row[1]]);
        new_row[len-1] = is_trap(&[row[len-2], row[len-1], false]);
        for tile in 1..len-1 {
            new_row[tile] = is_trap(&row[tile-1..=tile+1]);
        }

        row = new_row;
        count += row.iter().filter(|b| !*b).count();
    }
    println!("{}", count);
}

fn is_trap(prev: &[bool]) -> bool {
    match prev {
        [true, true, false] => true,
        [false, true, true] => true,
        [true, false, false] => true,
        [false, false, true] => true,
        [_, _, _] => false,
        _ => panic!(),
    }
}
