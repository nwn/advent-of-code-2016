use std::io::{self, prelude::*};
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut program = vec![];
    for line in lines {
        let words: Vec<_> = line.split_whitespace().collect();
        match words[0] {
            "cpy" => {
                let dest = to_reg(words[2]);
                if let Ok(lit) = words[1].parse::<i32>() {
                    program.push(Cpy(Lit(lit), dest));
                } else {
                    let src = to_reg(words[1]);
                    program.push(Cpy(Reg(src), dest));
                }
            },
            "inc" => {
                program.push(Inc(to_reg(words[1])));
            },
            "dec" => {
                program.push(Dec(to_reg(words[1])));
            },
            "jnz" => {
                let cond = if let Ok(lit) = words[1].parse::<i32>() {
                    Lit(lit)
                } else {
                    Reg(to_reg(words[1]))
                };
                let amnt = if let Ok(lit) = words[2].parse::<i32>() {
                    Lit(lit)
                } else {
                    Reg(to_reg(words[2]))
                };
                program.push(Jnz(cond, amnt));
            },
            "tgl" => {
                if let Ok(lit) = words[1].parse::<i32>() {
                    program.push(Tgl(Lit(lit)));
                } else {
                    let reg = to_reg(words[1]);
                    program.push(Tgl(Reg(reg)));
                }
            },
            _ => panic!("Unknown instruction: {}", line),
        }
    }

    let mut registers = [7, 0, 0, 0];
    let mut pc = 0;
    while let Some(&instr) = program.get(pc as usize) {
        let mut step = 1;
        match instr {
            Cpy(Lit(lit), dest) => registers[dest] = lit,
            Cpy(Reg(src), dest) => registers[dest] = registers[src],
            Inc(reg) => registers[reg] += 1,
            Dec(reg) => registers[reg] -= 1,
            Jnz(Lit(lit), Lit(amnt)) => if lit != 0 { step = amnt; },
            Jnz(Lit(lit), Reg(amnt)) => if lit != 0 { step = registers[amnt]; },
            Jnz(Reg(reg), Lit(amnt)) => if registers[reg] != 0 { step = amnt; },
            Jnz(Reg(reg), Reg(amnt)) => if registers[reg] != 0 { step = registers[amnt]; },
            Tgl(val) => {
                let amnt = match val {
                    Lit(lit) => lit,
                    Reg(reg) => registers[reg],
                };

                if let Some(instr) = program.get_mut((pc + amnt) as usize) {
                    *instr = match *instr {
                        Cpy(val, dest) => Jnz(val, Reg(dest)),
                        Inc(reg) => Dec(reg),
                        Dec(reg) => Inc(reg),
                        Tgl(Lit(_)) => panic!(),
                        Tgl(Reg(reg)) => Inc(reg),
                        Jnz(_, Lit(_)) => panic!(),
                        Jnz(val, Reg(reg)) => Cpy(val, reg),
                    };
                }
            },
        }
        pc += step;
    }

    println!("{}", registers[0]);
}

fn to_reg(str: &str) -> usize {
    (str.as_bytes()[0] - b'a') as usize
}

use Val::*;
#[derive(Copy, Clone)]
enum Val {
    Lit(i32),
    Reg(usize),
}

use Instr::*;
#[derive(Copy, Clone)]
enum Instr {
    Cpy(Val, usize),
    Inc(usize),
    Dec(usize),
    Jnz(Val, Val),
    Tgl(Val),
}
