use std::io::{self, prelude::*};
use std::collections::HashSet;
fn main() {
    let stdin = io::stdin();
    let line = stdin.lock().lines().map(Result::unwrap).next().unwrap();
    let words: Vec<_> = line.split_whitespace().flat_map(|word| word.split_terminator(',')).collect();

    let mut visited = HashSet::new();
    let (mut x, mut y): (i32, i32) = (0, 0);
    let mut dir = Up;
    visited.insert((x,y));
    'outer: for word in words {
        match &word[..1] {
            "L" => dir.turn_left(),
            "R" => dir.turn_right(),
            _ => panic!("Unexpected intruction: {}", word),
        }
        let dist = word[1..].parse().unwrap();
        for _ in 0..dist {
            match dir {
                Up    => y += 1,
                Down  => y -= 1,
                Left  => x -= 1,
                Right => x += 1,
            }
            if !visited.insert((x,y)) {
                break 'outer;
            }
        }
    }

    println!("{}", x.abs() + y.abs());
}

use Dir::*;
enum Dir { Up, Down, Left, Right, }
impl Dir {
    fn turn_left(&mut self) {
        match *self {
            Up    => *self = Left,
            Down  => *self = Right,
            Left  => *self = Down,
            Right => *self = Up,
        }
    }
    fn turn_right(&mut self) {
        match *self {
            Up    => *self = Right,
            Down  => *self = Left,
            Left  => *self = Up,
            Right => *self = Down,
        }
    }
}
