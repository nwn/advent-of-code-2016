use std::io::{self, prelude::*};
use std::default::Default;
fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);

    let mut screen = Matrix::new(6, 50);
    for line in lines {
        let words: Vec<_> = line.split_whitespace().collect();
        let words: Vec<_> = words.into_iter().flat_map(|word| word.split(|ch: char| ch == '=' || ch == 'x')).collect();
        match (words[0], words[1]) {
            ("rect", _) => {
                let cols = words[1].parse().unwrap();
                let rows = words[2].parse().unwrap();
                for row in 0..rows {
                    for col in 0..cols {
                        screen[row][col] = true;
                    }
                }
            },
            ("rotate", "row") => {
                let row = words[3].parse().unwrap();
                let amnt = screen.cols() - words[5].parse::<usize>().unwrap();

                let mut vals = Vec::with_capacity(screen.cols());
                for col in 0..screen.cols() {
                    vals.push(screen[row][col]);
                }
                for col in 0..screen.cols() {
                    screen[row][col] = vals[(col + amnt) % screen.cols()];
                }
            },
            ("rotate", "column") => {
                let col = words[4].parse::<usize>().unwrap();
                let amnt = screen.rows() - words[6].parse::<usize>().unwrap();

                let mut vals = Vec::with_capacity(screen.rows());
                for row in 0..screen.rows() {
                    vals.push(screen[row][col]);
                }
                for row in 0..screen.rows() {
                    screen[row][col] = vals[(row + amnt) % screen.rows()];
                }
            },
            _ => panic!("Unrecognized command: {}", line),
        }
    }

    for row in 0..screen.rows() {
        for col in 0..screen.cols() {
            if screen[row][col] {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!("");
    }
}

struct Matrix<T> {
    data: Vec<T>,
    cols: usize,
}
impl<T: Clone + Default> Matrix<T> {
    fn new(rows: usize, cols: usize) -> Self {
        Self {
            data: vec![T::default(); rows * cols],
            cols,
        }
    }
    fn rows(&self) -> usize {
        self.data.len() / self.cols
    }
    fn cols(&self) -> usize {
        self.cols
    }
}
impl<T> std::ops::Index<usize> for Matrix<T> {
    type Output = [T];
    fn index(&self, idx: usize) -> &Self::Output {
        let start = self.cols * idx;
        let end = self.cols * (idx + 1);
        &self.data[start..end]
    }
}
impl<T> std::ops::IndexMut<usize> for Matrix<T> {
    fn index_mut(&mut self, idx: usize) -> &mut Self::Output {
        let start = self.cols * idx;
        let end = self.cols * (idx + 1);
        &mut self.data[start..end]
    }
}
